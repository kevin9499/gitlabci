let express   = require('express')
let morgan    = require('morgan')
let config    = require('./config')
let mongoose = require('mongoose')
cors = require('cors');
bodyParser = require('body-parser');



const app     = express()
const port    = config.express.port

app.use(morgan('dev'))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false, parameterLimit: 50000 }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());
app.get('/', (req, res) => {
    res.redirect('/home')
})
app.use(express.json())
app.get('/home', (req, res) => {
    res.send('Hello World')
})

app.listen(port, () => console.log('Server started on port '+port))

// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect(
    'mongodb+srv://kevin94:kevin94@cluster0.jzrm5.mongodb.net/myFirstDatabase?retryWrites=true&w=majority');
app.use(bodyParser.urlencoded({ limit: '50mb', extended: false, parameterLimit: 50000 }));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(cors());
let post_routes = require('./routes/PostRoutes'); //importing route
post_routes(app); //register the route

let category_routes = require('./routes/CategoryRoutes'); //importing route
category_routes(app); //register the route